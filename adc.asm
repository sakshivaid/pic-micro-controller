; PROJEC TITLE: ANALOG TO DIGITAL CONVERSION
; ABSTRACT : PROJECT TO CONVERT ANALOG VOLTAGE TO DIGITAL AND DIPLAYING IT 
; DONE BY : RENE SALIH     AM.EN.U4CSE13149
; 		  : SAKSHI VAID    AM.EN.U4CSE13152
;		  : SREELAKSHMI S  AM.EN.U4CSE13152

		LIST	 p=16F877a			
		include "P16F877a.inc"		
	

		org		0x00										
		goto	start
		org		0x20


start								
		clrf	PORTB			

		banksel TRISC  					
		movlw   0x80
		movwf	TRISC					; Configure RC7/RX as input pin	and others as outputs
		CLRF 	TRISB
	 	
		BANKSEL TXSTA
		bsf 	TXSTA,TXEN 			
		bcf		TXSTA,SYNC 			
		bsf		TXSTA,BRGH 				
		bcf 	TXSTA,TX9				

		BANKSEL SPBRG					
		movlw	0x19  				
		movwf 	SPBRG 					
											
		banksel RCSTA  						
		bsf 	RCSTA,SPEN								
		bcf 	RCSTA,RX9			
		bsf 	RCSTA,CREN				



		bcf		STATUS, RP1			
		bsf		STATUS, RP0
		clrf	TRISB			

		movlw 	b'00000011'
		movwf	TRISA			

		movlw	b'00000111'			
		movwf	OPTION_REG

		movlw	B'00001110'			
		
		movwf	ADCON1				

		bcf		STATUS, RP0			
		movlw	B'11000001'			
		movwf	ADCON0

		movlw   0x80 		   			; 80(Hex)=128(Decimal), DDRAM address Reset display to 00
		call 	sendCMD					; i.e, which moves cursor to zeroth row zeroth column.		
		call    Delay2					; Delay routine should be called 2 times. Let LCD settle down. 
		call	Delay2	
		
	 	
	
Loop
		
checkTMR0
		btfss	INTCON,TMR0IF		
		goto	checkTMR0
		bcf		INTCON,TMR0IF
		bsf		ADCON0,GO		

Wait
		btfss	PIR1,ADIF		
		goto	Wait		
		bcf		PIR1,ADIF
		movfW	ADRESH			
		MOVWF 	PORTB
        
									
		call 	SendToLCD
		movlw   0x80 		   			; 80(Hex)=128(Decimal), DDRAM address Reset display to 00
		call 	sendCMD					; i.e, which moves cursor to zeroth row zeroth column.		

		clrf	TMR0				
		goto	Loop				







sendCMD	movwf 	0X42    	  		
	   	movlw	0xFE         		
	   	call 	SendToLCD
	   	movfW 	0X42	          
	    CALL 	SendToLCD
        RETURN       



SendToLCD
	    bcf     STATUS,RP1      		
	    bcf     STATUS,RP0
        MOVWF 0X50
		ANDLW 0XF0
		MOVWF 0X51
		SWAPF 0X51,0
		ADDLW 0X30
		MOVWF TXREG	
		MOVF   0X50, 0
		ANDLW   0X0F
        ADDLW 	0X30	
		movwf   TXREG           	
		CALL Delay2
		MOVLW 0X0D
		MOVWF TXREG
		
	
	 
        bsf     STATUS,RP0      	

putcheck
	    btfss   TXSTA,1    	  			 							   								   		 
        goto    putcheck        	 
        bcf     STATUS,RP0      		 
        return




Delay2	bcf 	STATUS, RP1				
		bcf 	STATUS, RP0	
		movlw 	d'252'			
		movwf 	0X40
J:		movwf 	0X41
K:		decfsz 	0X41,f
		goto 	K
		decfsz 	0X40,f
		goto 	J
		return

		end						
